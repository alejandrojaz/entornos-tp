<?php
	require "header.php";
	$isValid = false;
	if(!empty($_SESSION['carrito']) && $_SESSION['tipo_usuario'] == "cliente"){
		$isValid = true;
		$id_usuario = $_SESSION['id'];
		$fecha_pedido = date('Y-m-d');
		$insert = mysqli_query($con, "insert into pedido (id_usuario,fecha_pedido) values ('$id_usuario','$fecha_pedido')");
		// Selecciono pedido
		$pedido_query = mysqli_query($con, "select * from pedido where id_usuario=$id_usuario order by id desc limit 0,1");
		mysqli_data_seek($pedido_query, 0);
		$pedido = mysqli_fetch_assoc($pedido_query);
		$id_pedido = $pedido["id"];
		foreach($_SESSION['carrito'] as $k => $v){
			$cantidad = $v['cantidad'];
			$precio = $v['precio'];
			$insert = mysqli_query($con, "insert into pedido_pelicula (id_pedido,id_pelicula,cantidad,precio) values ('$id_pedido','$k','$cantidad','$precio')");
		}
		unset($_SESSION['carrito']);
	}
?>
	<div class="container">
		<?php
			if($isValid == false){
		?>
			<h1 class="d-block w-100">No se pudo finalizar la compra</h1>
			<div class="line d-block"></div>
			<br />
			<label class="error">Intent&oacute; realizar una compra de 0 productos</label><br />
			<label class="error">La invitamos a recorrer nuestro sitio web, para buscar pel&iacute;culas de su inter&eacute;s</label><br />
		<?php
			}
			else{
		?>
			<h1 class="d-block w-100">Compra finalizada</h1>
			<div class="line d-block"></div>
			<br />
			<label class="success">Se ha registrado su compra</label><br />
			<label class="success">Gracias por confiar en RetroMovies</label><br />
		<?php
			}
		?>
	</div>
<?php
	require "footer.php";
?>