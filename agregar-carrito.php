<?php
	session_start();
	include "conexion.php";
	$result = "false";
	// Creo sesión para el carrito si no existe
	if(!isset($_SESSION['carrito'])){
		$_SESSION['carrito'] = array();
	}
	if($_POST['id_pelicula']){
		// Valido que exista la película
		$id_pelicula = $_POST['id_pelicula'];
		$pelicula_query = mysqli_query($con, "select * from pelicula where id=$id_pelicula");
		if(mysqli_num_rows($pelicula_query) > 0){
			mysqli_data_seek($pelicula_query, 0);
			$pelicula = mysqli_fetch_assoc($pelicula_query);
			if(!isset($_SESSION['carrito'][$id_pelicula])){
				$_SESSION['carrito'][$id_pelicula] = array('cantidad' => 0, 'precio' => $pelicula['precio']);
			}
			// Agrego una a la cantidad pedida
			$_SESSION['carrito'][$id_pelicula]['cantidad']++;
			$result = "success";
		}
	}
	echo $result;
?>