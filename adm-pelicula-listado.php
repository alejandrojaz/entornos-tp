<?php
	require "header.php";
	// Si intenta acceder un usuario que no sea el administrador lo redireccionamos a la página de inicio
	if($_SESSION['tipo_usuario'] != 'administrador'){
		header("Location: 404.php");
	}
	else{
		// Si enviaron un ID, significa que están eliminando un género
		if(isset($_GET['id'])){
			$id = $_GET['id'];
			$delete = mysqli_query($con, "delete from pelicula where id='$id'");
			if($delete){
				$claseMensajeDelete = "success";
				$mensajeDelete = "Pel&iacute;cula eliminada correctamente";
			}
			else{
				$claseMensajeDelete = "error";
				$mensajeDelete = "Error al eliminar la pel&iacute;cula intente nuevamente";
			}
		}
		$peliculas_query = mysqli_query($con, "select * from pelicula order by id asc");
?>
	<div class="align-center">
		<div class="btn-group admin-menu" role="group">
		  <a href="adm-genero-listado.php" class="btn btn-secondary">G&eacute;neros</a>
		  <a href="adm-pelicula-listado.php" class="btn btn-secondary active">Pel&iacute;culas</a>
		  <a href="adm-usuario-listado.php" class="btn btn-secondary">Usuarios</a>
		  <a href="adm-pedidos-listado.php" class="btn btn-secondary">Ver pedidos</a>
		</div>
	</div>
	<div class="container">
		<h1 class="d-block w-100">Listado de Pel&iacute;culas</h1>
		<div class="line d-block"></div>
		<?php
			if(isset($mensajeDelete)){
				echo "<label class='" . $claseMensajeDelete . "'>" . $mensajeDelete . "</label>";
			}
		?>
		<div class="align-right">
			<a href="adm-pelicula-agregar.php" class="btn btn-secondary active">AGREGAR PEL&Iacute;CULA</a>
		</div>
		<br />
		<table class="table" border="0">
			<thead class="thead-light">
				<tr>
					<th scope="col" class="w100">ID</th>
					<th scope="col">Nombre</th>
					<th scope="col" class="w100">Acciones</th>
				</tr>
			</thead>
			<tbody>
				<?php
					while($pelicula = mysqli_fetch_assoc($peliculas_query)){
						echo "<tr><td>" . $pelicula['id'] . "</td><td>" . $pelicula['nombre'] . "</td><td class='align-right'><a href='adm-pelicula-listado.php?id=" . $pelicula['id'] . "' title='Eliminar' onClick='return confirm(\"Seguro desea eliminar la pel&iacute;cula?\");'><i class='fa fa-trash-alt'></i></a>&nbsp;<a href='adm-pelicula-editar.php?id=" . $pelicula['id'] . "' title='Editar'><i class='fa fa-edit'></i></a></td></tr>";
					}
				?>
			</tbody>
		</table>
	</div>
<?php
}
	require "footer.php";
?>
