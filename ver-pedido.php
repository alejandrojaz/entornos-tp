<?php
	require "header.php";
	$isValid = false;
	if(isset($_GET['id'])){
		$id = $_GET['id'];
		// Valido que si el que ingresa a ver el pedido es un cliente, que dicho pedido realmente le corresponda
		if($_SESSION['tipo_usuario'] == "cliente"){
			$id_usuario = $_SESSION['id'];
			$pedido_query = mysqli_query($con, "select * from pedido where id_usuario='$id_usuario' and id='$id'");
			if(mysqli_num_rows($pedido_query) > 0){
				$isValid = true;
			}
		}
		if($_SESSION['tipo_usuario'] == "administrador"){
			$pedido_query = mysqli_query($con, "select * from pedido where id='$id'");
			// Si es administrador, siempre será válido
			$isValid = true;
		}
	}
	if($isValid == false){
		header("Location: 404.php");
	}
	else{
		$pedido_pelicula_query = mysqli_query($con, "select pedido_pelicula.id_pelicula, pedido_pelicula.cantidad, pedido_pelicula.precio, p.nombre from pedido_pelicula inner join pelicula p on p.id = pedido_pelicula.id_pelicula where pedido_pelicula.id_pedido='$id'");
		$total = 0;
?>
	<div class="container">
		<h1 class="d-block w-100">Ver pedido nro: <?php echo $_GET['id']; ?></h1>
		<div class="line d-block"></div>
		<table class="table mt-4" border="0">
			<thead class="thead-light">
				<tr>
					<th scope="col" class="w100">Cantidad</th>
					<th scope="col" class="w150">Precio individual</th>
					<th scope="col" class="w100">Precio</th>
					<th scope="col">Nombre</th>
				</tr>
			</thead>
			<tbody>
				<?php
					while($pedido_pelicula = mysqli_fetch_assoc($pedido_pelicula_query)){
						$cantidad = $pedido_pelicula['cantidad'];
						$precio_individual = $pedido_pelicula['precio'];
						$precio = round($precio_individual * $cantidad, 2);
						$total += $precio;
						echo "<tr><td>" . $cantidad . "</td><td>$ " . $precio_individual . "</td><td>$ " . $precio . "</td><td>" . $pedido_pelicula['nombre'] . "</td></tr>";
					}
				?>
			</tbody>
		</table>
		<?php
			if($total != 0){
				echo "<label>Total: $ " . $total . "</label><br />";
			}
		?>
		<br />
	</div>
<?php
	}
	require "footer.php";
?>