<?php
	require "header.php";
	// Si intenta acceder un usuario que no sea el administrador lo redireccionamos a la página de inicio
	if($_SESSION['tipo_usuario'] != 'administrador'){
		header("Location: 404.php");
	}
	else{
		if(isset($_POST['agregar'])){
			$id_genero = $_POST['id_genero'];
			$nombre = $_POST['nombre'];
			$sinopsis = $_POST['sinopsis'];
			$director = $_POST['director'];
			$origen = $_POST['origen'];
			$lanzamiento = $_POST['lanzamiento'];
			$trailer = $_POST['trailer'];
			$portada = $_POST['portada'];
			$precio = $_POST['precio'];
			$insert = mysqli_query($con, "insert into pelicula (id_genero, nombre, sinopsis, director, origen, lanzamiento, trailer,  portada, precio) values ('$id_genero','$nombre','$sinopsis','$director','$origen','$lanzamiento', '$trailer',  '$portada', '$precio')");
			if($insert){
				$claseMensajeInsert = "success";
				$mensajeInsert = "Pel&iacute;cula agregado correctamente";
			}
			else{
				$claseMensajeInsert = "error";
				$mensajeInsert = "Error al agregar pel&iacute;cula, intente nuevamente";
			}
		}
		$generos_query = mysqli_query($con, "select * from genero order by nombre asc");
?>
	<div class="align-center">
		<div class="btn-group admin-menu" role="group">
		  <a href="adm-genero-listado.php" class="btn btn-secondary">G&eacute;neros</a>
		  <a href="adm-pelicula-listado.php" class="btn btn-secondary active">Pel&iacute;culas</a>
		  <a href="adm-usuario-listado.php" class="btn btn-secondary">Usuarios</a>
		  <a href="adm-pedidos-listado.php" class="btn btn-secondary">Ver pedidos</a>
		</div>
	</div>
	<div class="container">
		<h1 class="d-block w-100">Agregar pel&iacute;cula</h1>
		<div class="line d-block"></div>
		<?php
			if(isset($mensajeInsert)){
				echo "<label class='" . $claseMensajeInsert . "'>" . $mensajeInsert . "</label>";
			}
		?>
		<form method="post" action="">
			<div class="form-group col-12">
				<label for="nombre">Nombre</label>
				<input type="text" class="form-control" id="nombre" placeholder="Nombre..." required="required" name="nombre" maxlength="50" />
			</div>
			<div class="form-group col-12">
				<label for="id_genero">G&eacute;nero</label>
				<select class="form-control" name="id_genero" id="id_genero">
					<?php
						while($genero = mysqli_fetch_assoc($generos_query)){
							echo "<option value='" . $genero['id'] . "'>" . $genero['nombre'] ."</option>";
						}
					?>
				</select>
			</div>
			<div class="form-group col-12">
				<label for="sinopsis">Sinopsis</label>
				<textarea class="form-control" name="sinopsis" id="sinopsis" placeholder="Sinopsis" rows="3"></textarea>
			</div>
			<div class="form-group col-12">
				<label for="sinopsis">Trailer (iframe youtube)</label>
				<textarea class="form-control" name="trailer" id="trailer" placeholder="Trailer (iframe youtube)" rows="3"></textarea>
			</div>
			<div class="form-group col-12">
				<label for="portada">Portada (url imagen)</label>
				<input type="text" class="form-control" id="portada" placeholder="Portada..." required="required" name="portada" maxlength="255" />
			</div>
			<div class="form-group col-12">
				<label for="director">Director</label>
				<input type="text" class="form-control" id="director" placeholder="Director..." required="required" name="director" maxlength="50" />
			</div>
			<div class="form-group col-12">
				<label for="origen">Origen</label>
				<input type="text" class="form-control" id="origen" placeholder="Origen..." required="required" name="origen" maxlength="50" />
			</div>
			<div class="form-group col-12">
				<label for="lanzamiento">Lanzamiento</label>
				<input type="text" class="form-control" id="lanzamiento" placeholder="Lanzamiento..." required="required" name="lanzamiento" maxlength="4" />
			</div>
			<div class="form-group col-12">
				<label for="precio">Precio</label>
				<input type="text" class="form-control" id="precio" placeholder="Precio..." required="required" name="precio"/>
			</div>
			<div class="form-group col-12 align-right">
				<a href="adm-pelicula-listado.php" class="btn">VOLVER</a>
				<button type="submit" class="btn" name="agregar">GUARDAR</button>
			</div>
		</form>
	</div>
<?php
	}
	require "footer.php";
?>
