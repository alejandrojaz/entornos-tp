<?php
	require "header.php";
	// Vericifamos si se envió el formulario de login
	if(isset($_POST['ingreso'])) {
		// Buscamos al usuario, si existe guardamos en sesión el tipo de usuario y redireccionamos a la página de inicio, sino guardamos mensaje de error 
		$email = $_POST['email'];
		$password = md5($_POST['password']);
		$usuario_query = mysqli_query($con, "select id, tipo from usuario where email='$email' and password='$password'");
		if(mysqli_num_rows($usuario_query) > 0){
			mysqli_data_seek($usuario_query, 0);
			$usuario = mysqli_fetch_assoc($usuario_query);
			$_SESSION['id'] = $usuario['id'];
			$_SESSION['tipo_usuario'] = $usuario['tipo'];
			header("Location: index.php");
		}
		else {
			$mensajeLogin = "El usuario y/o la contrase&ntilde;a son incorrectos";
		}
	}
	// Vericifamos si se envió el formulario de registro
	if(isset($_POST['registro'])){
		$email = $_POST['email'];
		$nombre = $_POST['nombre'];
		$apellido = $_POST['apellido'];
		$password = md5($_POST['password']);
		$fecha_nacimiento = $_POST['fecha_nacimiento'];
		$provincia = $_POST['provincia'];
		$localidad = $_POST['localidad'];
		$direccion = $_POST['direccion'];
		$telefono = $_POST['telefono'];
		$usuario = mysqli_query($con, "select * from usuario where email='$email'");
		$claseMensajeRegistro = "error";
		if(mysqli_num_rows($usuario) > 0){
			$mensajeRegistro = "El mail ingresado ya se encuentra registrado";
		}
		else{
			$insert = mysqli_query($con, "insert into usuario (nombre,apellido,email,password,fecha_nacimiento,provincia,localidad,direccion,telefono) values ('$nombre','$apellido','$email','$password','$fecha_nacimiento','$provincia','$localidad','$direccion','$telefono')");
			if($insert){
				$claseMensajeRegistro = "success";
				$mensajeRegistro = "Usuario registrado correctamente";
			}
			else{
				$mensajeRegistro = "Error al registrar usuario, intente nuevamente";
			}
		}
	}
?>
	<div class="container row">
		<div class="col-xl-6 col-lg-12">
			<h1 class="d-block w-100">Ingresar</h1>
			<div class="line d-block"></div>
			<?php
				if(isset($mensajeLogin)){
					echo "<label class='error'>" . $mensajeLogin . "</label>";
				}
			?>
			<form id="frm-login" method="post" action="ingreso.php">
				<div class="form-group col-12">
					<label for="email">Correo Electr&oacute;nico</label>
					<input type="email" class="form-control" id="login-email" placeholder="Correo..." required="required" name="email" maxlength="50" />
				</div>
				<div class="form-group col-12">
					<label for="password">Contrase&ntilde;a</label>
					<input type="password" class="form-control" id="login-password" required="required" name="password" />
				</div>
				<div class="form-group col-12">
					<button type="submit" class="btn" name="ingreso">INGRESAR</button>
				</div>
			</form>
		</div>
		<div class="col-xl-6 col-lg-12">
			<h1 class="d-block w-100">Registrarse</h1>
			<div class="line d-block"></div>
			<?php
				if(isset($mensajeRegistro)){
					echo "<label class='" . $claseMensajeRegistro . "'>" . $mensajeRegistro . "</label>";
				}
			?>
			<form id="frm-registro" method="post" action="ingreso.php">
				<div class="form-group col-12">
					<label for="email">Correo Electr&oacute;nico</label>
					<input type="email" class="form-control" id="registro-email" placeholder="Correo..." required="required" name="email" maxlength="50" />
				</div>
				<div class="form-group col-12">
					<label for="password">Contrase&ntilde;a</label>
					<input type="password" class="form-control" id="registro-password" required="required" name="password" />
				</div>
				<div class="form-group col-12">
					<label for="nombre">Nombre</label>
					<input type="text" class="form-control" id="registro-nombre" placeholder="Nombre..." required="required" name="nombre" maxlength="50" />
				</div>
				<div class="form-group col-12">
					<label for="apellido">Apellido</label>
					<input type="text" class="form-control" id="apellido" placeholder="Apellido..." required="required" name="apellido" maxlength="50" />
				</div>
				<div class="form-group col-12">
					<label for="fecha_nacimiento">Fecha de Nacimiento</label>
					<input type="date" class="form-control" id="fecha_nacimiento" required="required" name="fecha_nacimiento" />
				</div>
				<div class="form-group col-12">
					<label for="provincia">Provincia</label>
					<input type="text" class="form-control" id="provincia" placeholder="Provincia..." required="required" name="provincia" maxlength="30" />
				</div>
				<div class="form-group col-12">
					<label for="localidad">Localidad</label>
					<input type="text" class="form-control" id="localidad" placeholder="Localidad..." required="required" name="localidad" maxlength="50" />
				</div>
				<div class="form-group col-12">
					<label for="direccion">Direcci&oacute;n</label>
					<input type="text" class="form-control" id="direccion" placeholder="Direcci&oacute;n..." required="required" name="direccion" maxlength="20" />
				</div>
				<div class="form-group col-12">
					<label for="telefono">Tel&eacute;fono</label>
					<input type="text" class="form-control" id="telefono" placeholder="Tel&eacute;fono..." required="required" name="telefono" maxlength="20" />
				</div>
				<div class="form-group col-12">
					<button type="submit" class="btn" name="registro">Registrarse</button>
				</div>
			</form>
		</div>
	</div>
<?php
	require "footer.php";
?>

<script>
	$(document).on("change", "#fecha_nacimiento", function(){
		var fecha_nacimiento = new Date($('#fecha_nacimiento').val());
		var fecha_hoy = new Date();
		if(fecha_nacimiento >= fecha_hoy){
			alert('Fecha incorrecta, debe ser menor a la fecha actual');
			$('#fecha_nacimiento').val("");
		}
	});
</script>