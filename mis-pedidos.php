<?php
	require "header.php";
	// Si intenta acceder un usuario que no sea el administrador lo redireccionamos a la página de inicio
	if($_SESSION['tipo_usuario'] != 'cliente') {
		header("Location: 404.php");
	}
	else {
		$id_usuario = $_SESSION['id'];
		if(isset($_GET['id'])) {
			$id = $_GET['id'];
			// Valido que el pedido sea del usuario logueado
			$pedido_query = mysqli_query($con, "select * from pedido where id_usuario='$id_usuario' and id='$id'");
			if(mysqli_num_rows($pedido_query) > 0){
				$delete = mysqli_query($con, "delete from pedido where id='$id'");
				if($delete) {
					mysqli_query($con, "delete from pedido_pelicula where id_pedido='$id'");
					$claseMensajeDelete = "success";
					$mensajeDelete = "Pedido eliminado correctamente";
				}
				else {
					$claseMensajeDelete = "error";
					$mensajeDelete = "Error al eliminar el pedido, intente nuevamente";
				}
			}
		}
		$pedidos_query = mysqli_query($con, "select * from pedido where id_usuario='$id_usuario' order by id asc");
?>
<div class="container">
  <h1 class="d-block w-100">Mis Pedidos</h1>
  <div class="line d-block"></div>
  <?php
    if(isset($mensajeDelete)){
      echo "<label class='" . $claseMensajeDelete . "'>" . $mensajeDelete . "</label>";
    }
  ?>
	<br />
	<table class="table" border="0">
		<thead class="thead-light">
			<tr>
				<th scope="col" class="w100">Nro</th>
				<th scope="col" class="w150">Fecha de Pedido</th>
				<th scope="col">Estado del Pedido</th>
				<th scope="col" class="w100">Acciones</th>
			</tr>
		</thead>
		<tbody>
			<?php
				while($pedido = mysqli_fetch_assoc($pedidos_query)) {
					echo "<tr><td>" . $pedido['id'] . "</td><td>" . $pedido['fecha_pedido'] . "</td><td>" . $pedido['estado'] . "</td><td><a href='ver-pedido.php?id=" . $pedido['id'] . "' title='Ver-pedido'><i class='fa fa-search'></i></a>";
					if($pedido['estado'] == "pendiente"){
					  echo "&nbsp<a href='mis-pedidos.php?id=" . $pedido['id'] . "' title='Eliminar' onClick='return confirm(\"Seguro desea eliminar el pedido?\");' ><i class='fa fa-trash-alt'></i></a>";
					}
					echo "</td></tr>";
				}
			?>
		</tbody>
	</table>
</div>
<?php
	}
	require "footer.php";
?>
