<?php
	require "header.php";
	// Si intenta acceder un usuario que no sea el administrador lo redireccionamos a la página de inicio
	if($_SESSION['tipo_usuario'] != 'administrador'){
		header("Location: 404.php");
	}
	else{
		if(isset($_POST['agregar'])){
			$nombre = $_POST['nombre'];
			$insert = mysqli_query($con, "insert into genero (nombre) values ('$nombre')");
			if($insert){
				$claseMensajeInsert = "success";
				$mensajeInsert = "G&eacute;nero agregado correctamente";
			}
			else{
				$claseMensajeInsert = "error";
				$mensajeInsert = "Error al agregar g&eacute;nero, intente nuevamente";
			}
		}
?>
	<div class="align-center">
		<div class="btn-group admin-menu" role="group">
		  <a href="adm-genero-listado.php" class="btn btn-secondary active">G&eacute;neros</a>
		  <a href="adm-pelicula-listado.php" class="btn btn-secondary">Pel&iacute;culas</a>
		  <a href="adm-usuario-listado.php" class="btn btn-secondary">Usuarios</a>
		  <a href="adm-pedidos-listado.php" class="btn btn-secondary">Ver pedidos</a>
		</div>
	</div>
	<div class="container">
		<h1 class="d-block w-100">Agregar g&eacute;nero</h1>
		<div class="line d-block"></div>
		<?php
			if(isset($mensajeInsert)){
				echo "<label class='" . $claseMensajeInsert . "'>" . $mensajeInsert . "</label>";
			}
		?>
		<form method="post" action="">
			<div class="form-group col-12">
				<label for="nombre">Nombre</label>
				<input type="text" class="form-control" id="nombre" placeholder="Nombre..." required="required" name="nombre" maxlength="50" />
			</div>
				<div class="form-group col-12 align-right">
					<a href="adm-genero-listado.php" class="btn">VOLVER</a>
					<button type="submit" class="btn" name="agregar">GUARDAR</button>
				</div>
		</form>
	</div>
<?php
	}
	require "footer.php";
?>