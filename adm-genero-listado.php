<?php
	require "header.php";
	// Si intenta acceder un usuario que no sea el administrador lo redireccionamos a la página de inicio
	if($_SESSION['tipo_usuario'] != 'administrador'){
		header("Location: 404.php");
	}
	else{
		// Si enviaron un ID, significa que están eliminando un género
		if(isset($_GET['id'])){
			$id = $_GET['id'];
			$delete = mysqli_query($con, "delete from genero where id='$id'");
			if($delete){
				$claseMensajeDelete = "success";
				$mensajeDelete = "G&eacute;nero eliminado correctamente";
			}
			else{
				$claseMensajeDelete = "error";
				$mensajeDelete = "Error al eliminar el g&eacute;nero, intente nuevamente";
			}
		}
		$generos_query = mysqli_query($con, "select * from genero order by id asc");
?>
	<div class="align-center">
		<div class="btn-group admin-menu" role="group">
		  <a href="adm-genero-listado.php" class="btn btn-secondary active">G&eacute;neros</a>
		  <a href="adm-pelicula-listado.php" class="btn btn-secondary">Pel&iacute;culas</a>
		  <a href="adm-usuario-listado.php" class="btn btn-secondary">Usuarios</a>
		  <a href="adm-pedidos-listado.php" class="btn btn-secondary">Ver pedidos</a>
		</div>
	</div>
	<div class="container">
		<h1 class="d-block w-100">Listado de g&eacute;neros</h1>
		<div class="line d-block"></div>
		<?php
			if(isset($mensajeDelete)){
				echo "<label class='" . $claseMensajeDelete . "'>" . $mensajeDelete . "</label>";
			}
		?>
		<div class="align-right">
			<a href="adm-genero-agregar.php" class="btn btn-secondary active">AGREGAR G&Eacute;NERO</a>
		</div>
		<br />
		<table class="table" border="0">
			<thead class="thead-light">
				<tr>
					<th scope="col" class="w100">ID</th>
					<th scope="col">Nombre</th>
					<th scope="col" class="w100">Acciones</th>
				</tr>
			</thead>
			<tbody>
				<?php
					while($genero = mysqli_fetch_assoc($generos_query)){
						echo "<tr><td>" . $genero['id'] . "</td><td>" . $genero['nombre'] . "</td><td class='align-right'><a href='adm-genero-listado.php?id=" . $genero['id'] . "' title='Eliminar' onClick='return confirm(\"Seguro desea eliminar el g&eacute;nero?\");'><i class='fa fa-trash-alt'></i></a>&nbsp;<a href='adm-genero-editar.php?id=" . $genero['id'] . "' title='Editar'><i class='fa fa-edit'></i></a></td></tr>";
					}
				?>
			</tbody>
		</table>
	</div>
<?php
	}
	require "footer.php";
?>