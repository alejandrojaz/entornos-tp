<?php
	require "header.php";
?>
	<div class="container">
		<h1 class="d-block">Mapa del sitio</h1>
		<div class="line d-block"></div>
		<br /><br />
		<label>El mapa es mostrado seg&uacute;n el nivel de usuario actual (invitado, cliente o administrador)</label>
		<br /><br />
<?php
	if($_SESSION['tipo_usuario'] == "invitado"){
?>
	<ul>
		<li>1- Inicio: <a href="http://tpentornos.7kb.net/">http://tpentornos.7kb.net/</a></li>
		<li>2- Categor&iacute;as: <a href="http://tpentornos.7kb.net/genero.php?id=1">http://tpentornos.7kb.net/genero.php?id=ID (Cambiar ID por uno v&aacute;lido o click para ver ejemplo)</a></li>
		<li>3- Pel&iacute;culas: <a href="http://tpentornos.7kb.net/pelicula.php?id=1">http://tpentornos.7kb.net/pelicula.php?id=ID (Cambiar ID por uno v&aacute;lidoo click para ver ejemplo)</a></li>
		<li>4- Buscador: <a href="http://tpentornos.7kb.net/buscar.php">http://tpentornos.7kb.net/buscar.php</a></li>
		<li>5- Visualizaci&oacute;n carrito de compras: <a href="http://tpentornos.7kb.net/carrito.php">http://tpentornos.7kb.net/carrito.php</a></li>
		<li>6- Ingreso/registro de usuario: <a href="http://tpentornos.7kb.net/ingreso.php">http://tpentornos.7kb.net/ingreso.php</a></li>
	</ul>
<?php
	}
	if($_SESSION['tipo_usuario'] == "cliente"){
?>
	<ul>
		<li>1- Inicio: <a href="http://tpentornos.7kb.net/">http://tpentornos.7kb.net/</a></li>
		<li>2- Categor&iacute;as: <a href="http://tpentornos.7kb.net/genero.php?id=1">http://tpentornos.7kb.net/genero.php?id=ID (Cambiar ID por uno v&aacute;lido o click para ver ejemplo)</a></li>
		<li>3- Pel&iacute;culas: <a href="http://tpentornos.7kb.net/pelicula.php?id=1">http://tpentornos.7kb.net/pelicula.php?id=ID (Cambiar ID por uno v&aacute;lidoo click para ver ejemplo)</a></li>
		<li>4- Buscador: <a href="http://tpentornos.7kb.net/buscar.php">http://tpentornos.7kb.net/buscar.php</a></li>
		<li>5- Visualizaci&oacute;n carrito de compras: <a href="http://tpentornos.7kb.net/carrito.php">http://tpentornos.7kb.net/carrito.php</a></li>
		<li>6- Pedidos realizados por el usuario: <a href="http://tpentornos.7kb.net/mis-pedidos.php">http://tpentornos.7kb.net/mis-pedidos.php</a></li>
		<li>7- Detalles de pedido espec&iacute;fico: <a href="http://tpentornos.7kb.net/ver-pedido.php?id=1">http://tpentornos.7kb.net/ver-pedido.php?id=ID (cambiar ID por uno v&aacute;lido, no hay pedido activo al momento de haber hecho el mapa)</a></li>
	</ul>
<?php
	}
	if($_SESSION['tipo_usuario'] == "administrador"){
?>
	<ul>
		<li>1- Inicio: <a href="http://tpentornos.7kb.net/">http://tpentornos.7kb.net/</a></li>
		<li>2- Categor&iacute;as: <a href="http://tpentornos.7kb.net/genero.php?id=1">http://tpentornos.7kb.net/genero.php?id=ID (Cambiar ID por uno v&aacute;lido o click para ver ejemplo)</a></li>
		<li>3- Pel&iacute;culas: <a href="http://tpentornos.7kb.net/pelicula.php?id=1">http://tpentornos.7kb.net/pelicula.php?id=ID (Cambiar ID por uno v&aacute;lidoo click para ver ejemplo)</a></li>
		<li>4- Buscador: <a href="http://tpentornos.7kb.net/buscar.php">http://tpentornos.7kb.net/buscar.php</a></li>
		<li>5- Listado g&eacute;neros: <a href="http://tpentornos.7kb.net/adm-genero-listado.php">http://tpentornos.7kb.net/adm-genero-listado.php</a></li>
		<li>6- Alta de g&eacute;nero: <a href="http://tpentornos.7kb.net/adm-genero-agregar.php">http://tpentornos.7kb.net/adm-genero-agregar.php</a></li>
		<li>7- Edici&oacute;n de g&eacute;nero: <a href="http://tpentornos.7kb.net/adm-genero-editar.php?id=1">http://tpentornos.7kb.net/adm-genero-editar.php?id=ID (cambiar ID por uno v&aacute;lido o click para ver ejemplo)</a></li>
		<li>8- Listado pel&iacute;cula: <a href="http://tpentornos.7kb.net/adm-pelicula-listado.php">http://tpentornos.7kb.net/adm-pelicula-listado.php</a></li>
		<li>9- Alta de pel&iacute;cula: <a href="http://tpentornos.7kb.net/adm-pelicula-agregar.php">http://tpentornos.7kb.net/adm-pelicula-agregar.php</a></li>
		<li>10- Edici&oacute;n de pel&iacute;cula: <a href="http://tpentornos.7kb.net/adm-pelicula-editar.php?id=1">http://tpentornos.7kb.net/adm-pelicula-editar.php?id=ID (cambiar ID por uno v&aacute;lido o click para ver ejemplo)</a></li>
		<li>11- Listado usuarios: <a href="http://tpentornos.7kb.net/adm-usuario-listado.php">http://tpentornos.7kb.net/adm-usuario-listado.php</a></li>
		<li>12- Listado pedidos: <a href="http://tpentornos.7kb.net/adm-pedidos-listado.php">http://tpentornos.7kb.net/adm-pedidos-listado.php</a></li>
		<li>13- Detalles de pedido espec&iacute;fico: <a href="http://tpentornos.7kb.net/ver-pedido.php?id=1">http://tpentornos.7kb.net/ver-pedido.php?id=ID (cambiar ID por uno v&aacute;lido, no hay pedido activo al momento de haber hecho el mapa)</a></li>
	</ul>
<?php
	}
?>
	<br /><br />
	</div>
<?php
	require "footer.php";
?>