<?php
	require "header.php";
	// Si intenta acceder un usuario que no sea el administrador lo redireccionamos a la página de inicio
	if($_SESSION['tipo_usuario'] != 'administrador'){
		header("Location: 404.php");
	}
	else{
		// Si enviaron un ID, significa que están eliminando un género
		if(isset($_GET['id']) && isset($_GET['accion'])){
			$id = $_GET['id'];
			if($_GET['accion'] == "eliminar"){
				$delete = mysqli_query($con, "delete from usuario where id='$id'");
				if($delete){
					$claseMensajeAccion = "success";
					$mensajeAccion = "Usuario eliminado correctamente";
				}
				else{
					$claseMensajeAccion = "error";
					$mensajeAccion = "Error al eliminar el usuario, intente nuevamente";
				}
			}
			if($_GET['accion'] == "agregar-administrador" || $_GET['accion'] == "eliminar-administrador"){
				if($_GET['accion'] == "eliminar-administrador"){
					$update = mysqli_query($con, "update usuario set tipo='cliente' where id='$id'");	
				}
				else{
					$update = mysqli_query($con, "update usuario set tipo='administrador' where id='$id'");	
				}
				if($update){
					$claseMensajeAccion = "success";
					$mensajeAccion = "Permisos del usuario modificados correctamente";
				}
				else{
					$claseMensajeAccion = "error";
					$mensajeAccion = "Error al modificar los permisos del usuario, intente nuevamente";
				}
			}
		}
		$usuarios_query = mysqli_query($con, "select * from usuario order by id asc");
?>
	<div class="align-center">
		<div class="btn-group admin-menu" role="group">
		  <a href="adm-genero-listado.php" class="btn btn-secondary">G&eacute;neros</a>
		  <a href="adm-pelicula-listado.php" class="btn btn-secondary">Pel&iacute;culas</a>
		  <a href="adm-usuario-listado.php" class="btn btn-secondary active">Usuarios</a>
		  <a href="adm-pedidos-listado.php" class="btn btn-secondary">Ver pedidos</a>
		</div>
	</div>
	<div class="container">
		<h1 class="d-block w-100">Listado de usuarios</h1>
		<div class="line d-block"></div>
		<?php
			if(isset($mensajeAccion)){
				echo "<label class='" . $claseMensajeAccion . "'>" . $mensajeAccion . "</label>";
			}
		?>
		<br />
		<table class="table" border="0">
			<thead class="thead-light">
				<tr>
					<th scope="col" class="w100">ID</th>
					<th scope="col">Nombre</th>
					<th scope="col">Apellido</th>
					<th scope="col">Email</th>
					<th scope="col">Tel&eacute;fono/s</th>
					<th scope="col" class="w100">Acciones</th>
				</tr>
			</thead>
			<tbody>
				<?php
					while($usuario = mysqli_fetch_assoc($usuarios_query)){
					echo "<tr><td>" . $usuario['id'] . "</td><td>" . $usuario['nombre'] . "</td><td>" . $usuario['apellido'] . "</td><td>" . $usuario['email'] . "</td><td>" . $usuario['telefono'] . "</td>";
						// No dejamos que se auto elimine ni se saque los permisos de la administración
						if($usuario['id'] != $_SESSION['id']){
							echo "<td class='align-right'><a href='adm-usuario-listado.php?accion=eliminar&amp;id=" . $usuario['id'] . "' title='Eliminar' onClick='return confirm(\"Seguro desea eliminar el usuario?\");'><i class='fa fa-trash-alt'></i></a>&nbsp;";
							if($usuario['tipo'] == "administrador"){
								echo "<a href='adm-usuario-listado.php?accion=eliminar-administrador&amp;id=" . $usuario['id'] . "' title='Quitar permisos administrador' onClick='return confirm(\"Seguro desea quitar los permisos de administrador?\");'><i class='fa fa-user-times'></i></a>";
							}
							else{
								echo "<a href='adm-usuario-listado.php?accion=agregar-administrador&amp;id=" . $usuario['id'] . "' title='Agregar permisos administrador' onClick='return confirm(\"Seguro desea agregar permisos de administrador?\");'><i class='fa fa-user-plus'></i></a>";
							}
						}
						echo "</td></tr>";
					}
				?>
			</tbody>
		</table>
	</div>
<?php
	}
	require "footer.php";
?>