<?php
	require "header.php";
	// Por defecto
	$claseEnvio = "error";
	$mensajeEnvio = "Hubo un error al intentar enviar el mensaje, por favor intente nuevamente en unos instantes";
	if(isset($_POST['enviar'])){
		$body = "Hola " . $_POST['nombre'] . ", te agradecemos por ponerte en contacto con nosotros, te comentamos que recibimos tu consulta y en breve te responderemos";
		// También podríamos enviar al administrador, dejo ejemplo (queda excluído del alcance de este proyecto, ya que no configuramos una cuenta que le lleguen los correos):
		// mail("consultas@tpentornos.7kb.net", "RetroMovie - Consulta", "Mensaje con los datos traidos del formulario");
		if(mail($_POST['email'], "RetroMovie - Consulta", $body)){
			$claseEnvio = "success";
			$mensajeEnvio = "Gracias por ponerte en contacto con nosotros, en breve te responderemos.<br />Se ha enviado un mail a: " . $_POST['email'] . ", si no pudiste ver el mensaje, te pedimos revises la casilla de no deseados.";
		}
	}
?>
	<div class="container">
		<h1 class="d-block w-100">Contacto</h1>
		<div class="line d-block"></div>
		<br /><br />
		<label class="<?php echo $claseEnvio; ?>"><?php echo $mensajeEnvio; ?></label>
		<br /><br />
	</div>
<?php
	require "footer.php";
?>