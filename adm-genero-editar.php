<?php
	require "header.php";
	// Si intenta acceder un usuario que no sea el administrador lo redireccionamos a la página de inicio
	if($_SESSION['tipo_usuario'] != 'administrador'){
		header("Location: 404.php");
	}
	else{
		$isValid = false;
		if(isset($_GET['id'])){
			$id = $_GET['id'];
			if(isset($_POST['editar'])){
				$nombre = $_POST['nombre'];
				$update = mysqli_query($con, "update genero set nombre='$nombre' where id='$id'");
				if($update){
					$claseMensajeUpdate = "success";
					$mensajeUpdate = "G&eacute;nero editado correctamente";
				}
				else{
					$claseMensajeUpdate = "error";
					$mensajeUpdate = "Error al editar el g&eacute;nero, intente nuevamente";
				}
			}
			$genero_query = mysqli_query($con, "select * from genero where id='$id'");
			if(mysqli_num_rows($genero_query) > 0){
				$isValid = true;
				mysqli_data_seek($genero_query, 0);
				$genero = mysqli_fetch_assoc($genero_query);
			}
		}
?>
	<div class="align-center">
		<div class="btn-group admin-menu" role="group">
		  <a href="adm-genero-listado.php" class="btn btn-secondary active">G&eacute;neros</a>
		  <a href="adm-pelicula-listado.php" class="btn btn-secondary">Pel&iacute;culas</a>
		  <a href="adm-usuario-listado.php" class="btn btn-secondary">Usuarios</a>
		  <a href="adm-pedidos-listado.php" class="btn btn-secondary">Ver pedidos</a>
		</div>
	</div>
	<div class="container">
		<?php
		if($isValid == true){
		?>
			<h1 class="d-block w-100">Editar g&eacute;nero</h1>
			<div class="line d-block"></div>
			<?php
				if(isset($mensajeUpdate)){
					echo "<label class='" . $claseMensajeUpdate . "'>" . $mensajeUpdate . "</label>";
				}
			?>
			<form method="post" action="adm-genero-editar.php?id=<?php echo $genero['id']; ?>">
				<div class="form-group col-12">
					<label for="nombre">Nombre</label>
					<input type="text" class="form-control" id="nombre" value="<?php echo $genero['nombre']; ?>" required="required" name="nombre" maxlength="50" />
				</div>
					<div class="form-group col-12 align-right">
						<a href="adm-genero-listado.php" class="btn">VOLVER</a>
						<button type="submit" class="btn" name="editar">GUARDAR</button>
					</div>
			</form>
		<?php
		}
		else{
		?>
		<br />
		<label class="error">No se ha encontrado el g&eacute;nero que se intenta editar. Te invitamos a volver al listado o registrar un nuevo g&eacute;nero</label><br />
		<a href="adm-genero-listado.php" class="btn">VOLVER</a>
		<a href="adm-genero-agregar.php" class="btn">AGREGAR</a>
		<?php
		}
		?>
	</div>
<?php
	}
	require "footer.php";
?>