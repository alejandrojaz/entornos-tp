<?php
	require "header.php";
	$isValid = false;
	if(isset($_GET['id'])){
		$id_pelicula = $_GET["id"];
		$pelicula_query = mysqli_query($con, "select * from pelicula where id=$id_pelicula");
		if(mysqli_num_rows($pelicula_query) > 0){
			$isValid = true;
			mysqli_data_seek($pelicula_query, 0);
			$pelicula = mysqli_fetch_assoc($pelicula_query);
		}
	}
	if($isValid == true){
?>
	<div class="container">
		<h1 class="d-block w-100"><?php echo $pelicula['nombre'];?></h1>
		<div class="line d-block"></div>
		<p class="description"><?php echo $pelicula['sinopsis'];?></p>
		<h1 class="d-block w-100">Trailer</h1>
		<div class="line d-block mb-3"></div>
		<?php echo $pelicula['trailer'];?>
		<div class="mb-5">
			<h1 class="d-block w-100">Datos</h1>
			<div class="line d-block mb-3"></div>
			<ul class="datos">
				<li><strong>Director: </strong><?php echo $pelicula['director'];?> </li>
				<li><strong>A&ntilde;o: </strong> <?php echo $pelicula['lanzamiento'];?></li>
				<li><strong>Precio: </strong> $ <?php echo $pelicula['precio'];?></li>
			</ul>
		</div>
	</div>
	<?php
		if($_SESSION['tipo_usuario'] != "administrador"){
			echo '<button class="btn btn-comprar" type="button" data-id="' . $_GET['id'] . '">AGREGAR AL CARRITO</button>';
		}
	?>
<?php
	}
	else{
		header("Location: 404.php");
	}
	require "footer.php";
?>