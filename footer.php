		<footer>
			<div class="container row">
				<div class="col-xl-6 col-lg-12">
					<h2>Retro Movies</h2>
					<p><i class="fa fa-map-marker-alt"></i> Argentina, Santa Fe, Rosario</p>
					<p><i class="fab fa-whatsapp"></i> +5493413872631</p>
					<p><i class="fab fa-facebook"></i> /retromovies</p>
					<p><i class="fab fa-instagram"></i> /retromovies</p>
				</div>
				<div class="col-xl-6 col-lg-12">
					<form method="post" action="contacto.php" class="row">
						<div class="form-group col-xl-4 col-lg-12">
							<label for="nombre">Nombre y apellido</label>
							<input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre..." required="required" />
						</div>
						<div class="form-group col-xl-4 col-lg-12">
							<label for="email">E-mail</label>
							<input type="email" class="form-control" name="email" id="email" placeholder="E-mail..." required="required" />
						</div>
						<div class="form-group col-xl-4 col-lg-12">
							<label for="asunto">Asunto</label>
							<input type="text" class="form-control" name="asunto" id="asunto" placeholder="Asunto..." required="required" />
						</div>
						<div class="form-group col-12">
							<label for="mensaje">Mensaje</label>
							<textarea class="form-control" name="mensaje" id="mensaje"></textarea>
						</div>
						<div class="form-group col-12">
							<button name="enviar" type="submit" class="btn right">ENVIAR</button>
						</div>
					</form>
				</div>
			</div>
		</footer>
	</body>
</html>

<?php
	ob_end_flush();
?>