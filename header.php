<?php
	ob_start();
	header("Content-type: text/html;charset=ISO-8859-1");
	session_start();
	require "conexion.php";
	if(isset($_GET['accion'])){
		if($_GET['accion'] == "salir"){
			// Elimino usuario de la sesión
			unset($_SESSION['tipo_usuario']);
		}
	}
	if(!isset($_SESSION['tipo_usuario'])){
		$_SESSION['tipo_usuario'] = 'invitado';
	}
	// Creo sesión para el carrito si no existe
	if(!isset($_SESSION['carrito'])){
		$_SESSION['carrito'] = array();
	}
	$generos = mysqli_query($con, "select * from genero order by nombre asc");
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Retro Movies</title>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/style.css">
		<link rel="stylesheet" href="assets/css/responsive.css">
		<link rel="shortcut icon" type="image/jpg" href="assets/img/favicon.jpg">
		<script src="assets/js/jquery-1.10.2.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/functions.js"></script>
	</head>
	<body>
		<header>
			<nav class="navbar navbar-expand-lg">
				<div class="container">
					<a class="navbar-brand" href="index.php"><img src="assets/img/logo.png" class="logo" alt="Logo Retro Movie"></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false">
						<span class="fa fa-bars"></span>
					</button>
					<a class="sidebar-toggle" href="#"><i class="fa fa-bars"></i></a>
					<div class="navbar-collapse collapse" id="menu">
						<ul class="navbar-nav ml-auto">
							<?php
								while($genero = mysqli_fetch_assoc($generos)){
									echo '<li class="nav-item"><a href="genero.php?id=' . $genero['id'] .'" class="nav-link">' . $genero['nombre'] . '</a></li>';
								}
							?>
							<li class="nav-item"><a href="mapa.php" class="nav-link">Mapa del sitio</a></li>
							<li class="nav-item"><a href="buscar.php" class="nav-link" title="Buscar"><i class="fa fa-search"></i></a></li>
							<?php
								switch($_SESSION['tipo_usuario']){
									case "invitado": 	echo '<li class="nav-item"><a href="carrito.php" class="nav-link" title="Ver carrito de compras"><i class="fa fa-shopping-cart"></i></a></li>'; 
																echo '<li class="nav-item"><a href="ingreso.php" class="nav-link" title="Ingresar/Registrarme"><i class="fa fa-user"></i></a></li>'; break;
									case "cliente": echo '<li class="nav-item"><a href="carrito.php" class="nav-link"title="Ver carrito de compras"><i class="fa fa-shopping-cart"></i></a></li>';
															echo '<li class="nav-item"><a href="mis-pedidos.php" class="nav-link" title="Mis pedidos"><i class="fa fa-file-invoice"></i></a></li>';
															echo '<li class="nav-item"><a href="index.php?accion=salir" class="nav-link" title="Desconectarme"><i class="fa fa-sign-out-alt"></i></a></li>'; break;
									case "administrador": echo '<li class="nav-item"><a href="adm-genero-listado.php" class="nav-link" title="Administrar sitio"><i class="fa fa-user-tie"></i></a></li>';
																		echo '<li class="nav-item"><a href="index.php?accion=salir" class="nav-link" title="Desconectarme"><i class="fa fa-sign-out-alt"></i></a></li>'; break;
								}
							?>
						</ul>
					</div>
				</div>
			</nav>
		</header>
