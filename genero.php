<?php
	require "header.php";
	$isValid = false;
	if(isset($_GET['id'])){
		$id_genero = $_GET["id"];
		$genero_query = mysqli_query($con, "select nombre from genero where id=$id_genero");
		if(mysqli_num_rows($genero_query) > 0){
			$isValid = true;
			$limit = 18;
			// Valor por defecto para página y offset
			$offset = 0;
			$pagina = 0;
			if(isset($_GET['pagina'])){
				$pagina = $_GET['pagina'];
				$offset = $_GET['pagina'] * $limit;
			}
			$peliculas_totales = mysqli_query($con, "select * from pelicula where id_genero=$id_genero");
			$cantidad = mysqli_num_rows($peliculas_totales);
			$peliculas_query = mysqli_query($con, "select * from pelicula where id_genero=$id_genero limit $offset,$limit");
			mysqli_data_seek($genero_query, 0);
			$genero = mysqli_fetch_assoc($genero_query);
		}
	}
?>
	<div class="container">
		<?php
			if($isValid == true){
		?>
			<h1 class="d-block w-100"><?php echo $genero['nombre'];?></h1>
			<div class="line d-block"></div>
			<div class="row listado-peliculas">
				<?php
					while($pelicula = mysqli_fetch_assoc($peliculas_query)) {
				?>
					<div class="col-xl-2 col-lg-4">
						<a href="pelicula.php?id=<?php echo $pelicula['id']; ?>"><img src="<?php echo $pelicula['portada']; ?>" alt="<?php echo $pelicula['nombre']; ?>" /></a><br />
						<a href="pelicula.php?id=<?php echo $pelicula['id']; ?>"><?php echo $pelicula['nombre']; ?></a>
					</div>
				<?php
					}
					echo "<div class='container align-center mt-4'>";
						if($pagina != 0){
							$pagina_anterior = $pagina - 1;
							echo "<a href='genero.php?id=" . $_GET["id"] . "&amp;pagina=" . $pagina_anterior . "'>Anterior</a>&nbsp;";
						}
						if($limit + $offset < $cantidad){
							$pagina_siguiente = $pagina + 1;
							echo "&nbsp;<a href='genero.php?id=" . $_GET["id"] . "&amp;pagina=" . $pagina_siguiente . "'>Siguiente</a>";
						}
					echo "</div>";
				?>
			</div>
		<?php
			}
			else{
				header("Location: 404.php");
			}
		?>
	</div>
<?php
	require "footer.php";
?>