<?php
	require "header.php";
?>
	<div class="container">
		<h1 class="d-block w-100">P&aacute;gina no encontrada - Error</h1>
		<div class="line d-block"></div>
		<div class="align-center">
			<img src="assets/img/404.png" alt="Error 404" class="mt-5" />
		</div>
	</div>
<?php
	require "footer.php";
?>