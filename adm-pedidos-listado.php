<?php
	require "header.php";
	// Si intenta acceder un usuario que no sea el administrador lo redireccionamos a la página de inicio
	if($_SESSION['tipo_usuario'] != 'administrador') {
		header("Location: 404.php");
	}
	else {
		if(isset($_GET['accion']) and isset($_GET['id'])) {
			$id = $_GET['id'];
			$pedido_query = mysqli_query($con, "select pedido.fecha_pedido, u.email, u.nombre from pedido inner join usuario u on u.id = pedido.id_usuario where pedido.estado='pendiente' and pedido.id='$id'");
			if(mysqli_num_rows($pedido_query) > 0){
				mysqli_data_seek($pedido_query, 0);
				$pedido = mysqli_fetch_assoc($pedido_query);
				if(strtolower($_GET['accion']) == "eliminar") {
					$delete = mysqli_query($con, "delete from pedido where id='$id'");
					mysqli_query($con, "delete from pedido_pelicula where id_pedido='$id'");
					if($delete) {
						$claseMensajeAccion = "success";
						$mensajeAccion = "Pedido eliminado correctamente. Notificaci&oacute;n enviada al usuario";
						$body = "Hola " . $pedido['nombre'] . ", el pedido realizado con fecha " . $pedido['fecha_pedido'] . " fue cancelado, ante cualquier consulta, esperamos su contacto";
						mail($pedido['email'], "RetroMovie - Pedido cancelado", $body);
					}
					else {
						$claseMensajeAccion = "error";
						$mensajeAccion = "Error al eliminar el pedido, intente nuevamente";
					}
				}
				if(strtolower($_GET['accion']) == "confirmar") {
					$fecha_estimada = date("Y-m-d", strtotime(date("Y-m-d")."+7 days"));
					$update = mysqli_query($con, "update pedido set estado='enviado', fecha_estimada='$fecha_estimada' where id='$id'");
					if($update) {
						$claseMensajeAccion = "success";
						$mensajeAccion = "Pedido confirmado correctamente. Notificaci&oacute;n enviada al usuario";
						$body = "Hola " . $pedido['nombre'] . ", el pedido realizado con fecha " . $pedido['fecha_pedido'] . " fue confirmado, la fecha estimada de entrega es: " . $fecha_estimada . ", ante cualquier consulta, esperamos su contacto";
						mail($pedido['email'], "RetroMovie - Pedido confirmado", $body);
					}
					else {
						$claseMensajeAccion = "error";
						$mensajeAccion = "Error al confirmar el pedido, intente nuevamente";
					}
				}
			}
		}
		$id_usuario = $_SESSION['id'];
		$pedidos_query = mysqli_query($con, "select pedido.id, pedido.estado, pedido.fecha_pedido, u.nombre, u.apellido from pedido inner join usuario u on u.id = pedido.id_usuario order by id asc");
?>
	<div class="align-center">
		<div class="btn-group admin-menu" role="group">
		  <a href="adm-genero-listado.php" class="btn btn-secondary">G&eacute;neros</a>
		  <a href="adm-pelicula-listado.php" class="btn btn-secondary">Pel&iacute;culas</a>
		  <a href="adm-usuario-listado.php" class="btn btn-secondary">Usuarios</a>
		  <a href="adm-pedidos-listado.php" class="btn btn-secondary active">Ver pedidos</a>
		</div>
	</div>
	<div class="container">
	  <h1 class="d-block w-100">Pedidos</h1>
	  <div class="line d-block"></div>
	  <?php
		if(isset($mensajeAccion)){
		  echo "<label class='" . $claseMensajeAccion . "'>" . $mensajeAccion . "</label>";
		}
	  ?>
		<br />
		<table class="table" border="0">
			<thead class="thead-light">
				<tr>
					<th scope="col" class="w100">Nro</th>
					<th scope="col" class="w150">Fecha de Pedido</th>
					<th scope="col">Pedido por</th>
					<th scope="col">Estado del Pedido</th>
					<th scope="col" class="w100">Acciones</th>
				</tr>
			</thead>
			<tbody>
				<?php
					while($pedido = mysqli_fetch_assoc($pedidos_query)) {
						echo "<tr><td>" . $pedido['id'] . "</td><td>" . $pedido['fecha_pedido'] . "</td><td>" . $pedido['nombre'] . ", " . $pedido['apellido'] . "</td><td>" . $pedido['estado'] . "</td><td><a href='ver-pedido.php?id=" . $pedido['id'] . "' title='Ver-pedido'><i class='fa fa-search'></i></a>";
						if($pedido['estado'] == "pendiente"){
						  echo "&nbsp<a href='adm-pedidos-listado.php?accion=confirmar&id=" . $pedido['id'] . "' title='Confirmar' onClick='return confirm(\"Seguro desea confirmar y enviar el pedido?\");' ><i class='fa fa-check-circle'></i></a>&nbsp<a href='adm-pedidos-listado.php?accion=eliminar&id=" . $pedido['id'] . "' title='Eliminar' onClick='return confirm(\"Seguro desea eliminar el pedido?\");' ><i class='fa fa-trash-alt'></i></a>";
						}
						echo "</td></tr>";
					}
				?>
			</tbody>
		</table>
	</div>
<?php
	}
	require "footer.php";
?>
