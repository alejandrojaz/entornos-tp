<?php
	require "header.php";
	$peliculas_query = mysqli_query($con, "select * from pelicula order by id desc limit 0,6");
?>
	<div id="carouselHome" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
			<li data-target="#carouselHome" data-slide-to="0" class="active"></li>
			<li data-target="#carouselHome" data-slide-to="1"></li>
		</ol>
		<div class="carousel-inner">
			<div class="carousel-item active w-100" style="background-image: url('assets/img/slider1.jpg');">
			  <div class="carousel-caption uppercase">
				Los mejores cl&aacute;sicos al alcance de un click
			  </div>
			</div>
			<div class="carousel-item w-100" style="background-image: url('assets/img/slider2.jpg');">
			  <div class="carousel-caption uppercase">
				Tus mejores recuerdos, en VHS
			  </div>
			</div>
		</div>
		<a class="carousel-control-prev" href="#carouselHome" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselHome" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
	<div class="container">
		<h1 class="d-block w-100">Qui&eacute;nes Somos</h1>
		<div class="line d-block"></div>
		<p class="description">Somos una empresa que tiene una amplia variedad de pel&iacute;culas en VHS. Vendemos al mercado, copias de las mismas, hechas por nosotros. Ofrecemos una amplia variedad de cl&aacute;sicos, de variadas categor&iacute;as, como "Terror, Acci&oacute;n, Comedia" entre otras.</p>
	</div>
	<div class="container">
		<h1 class="d-block w-100">Novedades</h1>
		<div class="line d-block"></div>
		<div class="row listado-peliculas">
		<?php
			while($pelicula = mysqli_fetch_assoc($peliculas_query)) {
		?>
			<div class="col-xl-2 col-lg-4">
				<a href="pelicula.php?id=<?php echo $pelicula['id']; ?>"><img src="<?php echo$pelicula['portada']; ?>" alt="" /></a><br />
				<a href="pelicula.php?id=<?php echo $pelicula['id']; ?>"><?php echo $pelicula['nombre']; ?></a>
			</div>
		<?php
			}
		?>
		</div>
	</div>
<?php
	require "footer.php";
?>