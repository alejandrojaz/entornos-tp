<?php
	require "header.php";
	// Si intenta acceder un usuario que no sea el administrador lo redireccionamos a la página de inicio
	if($_SESSION['tipo_usuario'] != 'administrador'){
		header("Location: 404.php");
	}
	else{
		$isValid = false;
		if(isset($_GET['id'])){
			$id = $_GET['id'];
			if(isset($_POST['editar'])){
				$id_genero = $_POST['id_genero'];
				$nombre = $_POST['nombre'];
				$sinopsis = $_POST['sinopsis'];
				$director = $_POST['director'];
				$origen = $_POST['origen'];
				$lanzamiento = $_POST['lanzamiento'];
				$trailer = $_POST['trailer'];
				$portada = $_POST['portada'];
				$precio = $_POST['precio'];
				$update = mysqli_query($con, "UPDATE `pelicula` SET `id_genero` = '$id_genero', `nombre` = '$nombre', `director` = '$director', `sinopsis` = '$sinopsis', `origen` = '$origen', `lanzamiento` = '$lanzamiento',  `trailer` = '$trailer',   `portada` = '$portada', `precio` = '$precio' WHERE `id` = '$id'");
				if($update){
					$claseMensajeUpdate = "success";
					$mensajeUpdate = "Pel&iacute;cula editada correctamente";
				}
				else{
					$claseMensajeUpdate = "error";
					$mensajeUpdate = "Error al editar la pel&iacute;cula, intente nuevamente";
				}
			}
			$pelicula_query = mysqli_query($con, "select * from pelicula where id='$id'");
			if(mysqli_num_rows($pelicula_query) > 0){
				$isValid = true;
				mysqli_data_seek($pelicula_query, 0);
				$pelicula = mysqli_fetch_assoc($pelicula_query);
			}
		}
		$generos_query = mysqli_query($con, "select * from genero order by nombre asc");
?>
	<div class="align-center">
		<div class="btn-group admin-menu" role="group">
		  <a href="adm-genero-listado.php" class="btn btn-secondary">G&eacute;neros</a>
		  <a href="adm-pelicula-listado.php" class="btn btn-secondary active">Pel&iacute;culas</a>
		  <a href="adm-usuario-listado.php" class="btn btn-secondary">Usuarios</a>
		  <a href="adm-pedidos-listado.php" class="btn btn-secondary">Ver pedidos</a>
		</div>
	</div>
	<div class="container">
		<?php
		if($isValid == true){
		?>
			<h1 class="d-block w-100">Editar pel&iacute;cula</h1>
			<div class="line d-block"></div>
			<?php
				if(isset($mensajeUpdate)){
					echo "<label class='" . $claseMensajeUpdate . "'>" . $mensajeUpdate . "</label>";
				}
			?>
			<form method="post" action="adm-pelicula-editar.php?id=<?php echo $pelicula['id']; ?>">
				<div class="form-group col-12">
					<label for="nombre">Nombre</label>
					<input type="text" class="form-control" id="nombre" value="<?php echo $pelicula['nombre']; ?>" required="required" name="nombre" maxlength="50" />
				</div>
			<div class="form-group col-12">
				<label for="id_genero">G&eacute;nero</label>
				<select class="form-control" name="id_genero" id="id_genero">
					<?php
						while($genero = mysqli_fetch_assoc($generos_query)){
							$isSelected = "";
							if($genero['id'] == $pelicula['id_genero']) $isSelected = "selected";
							echo "<option value='" . $genero['id'] . "' " . $isSelected . ">" . $genero['nombre'] ."</option>";
						}
					?>
				</select>
			</div>
				<div class="form-group col-12">
					<label for="sinopsis">Sinopsis</label>
					<textarea class="form-control" name="sinopsis" id="sinopsis" placeholder="Sinopsis" rows="3"><?php echo $pelicula['sinopsis']; ?></textarea>
				</div>
			<div class="form-group col-12">
				<label for="sinopsis">Trailer (iframe youtube)</label>
				<textarea class="form-control" name="trailer" id="trailer" placeholder="Trailer (iframe youtube)" rows="3"><?php echo $pelicula['trailer']; ?></textarea>
			</div>
			<div class="form-group col-12">
				<label for="portada">Portada (url imagen)</label>
				<input type="text" class="form-control" id="portada" placeholder="Portada..." value="<?php echo $pelicula['portada']; ?>" required="required" name="portada" maxlength="255" />
			</div>
				<div class="form-group col-12">
					<label for="director">Director</label>
					<input type="text" class="form-control" id="director" value="<?php echo $pelicula['director']; ?>" required="required" name="director" maxlength="50" />
				</div>
				<div class="form-group col-12">
					<label for="origen">Origen</label>
					<input type="text" class="form-control" id="origen" value="<?php echo $pelicula['origen']; ?>" required="required" name="origen" maxlength="50" />
				</div>
				<div class="form-group col-12">
					<label for="lanzamiento">Lanzamiento</label>
					<input type="text" class="form-control" id="lanzamiento" value="<?php echo $pelicula['lanzamiento']; ?>" required="required" name="lanzamiento" maxlength="4" />
				</div>
				<div class="form-group col-12">
					<label for="precio">Precio</label>
					<input type="text" class="form-control" id="precio" value="<?php echo $pelicula['precio']; ?>" required="required" name="precio" />
				</div>
				<div class="form-group col-12 align-right">
					<a href="adm-pelicula-listado.php" class="btn">VOLVER</a>
					<button type="submit" class="btn" name="editar">GUARDAR</button>
				</div>
			</form>
		<?php
		}
		else{
		?>
		<br />
		<label class="error">No se ha encontrado la pel&iacute;cula que se intenta editar. Te invitamos a volver al listado o registrar un nuevo g&eacute;nero</label><br />
		<a href="adm-pelicula-listado.php" class="btn">VOLVER</a>
		<a href="adm-pelicula-agregar.php" class="btn">AGREGAR</a>
		<?php
		}
		?>
	</div>
<?php
	}
	require "footer.php";
?>
