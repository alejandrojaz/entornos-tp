<?php
	require "header.php";
	$isValid = false;
	$termino = "";
	if(isset($_POST['buscar'])){
		$termino = $_POST["termino"];
		$peliculas_query = mysqli_query($con, "select * from pelicula where nombre like '%$termino%' or sinopsis like '%$termino%'");
		if(mysqli_num_rows($peliculas_query) > 0){
			$isValid = true;
		}
		else{
			$mensajeBusqueda = "No se han encontrado pel&iacute;culas con el t&eacute;rmino buscado, por favor intente otro t&eacute;rmino.";
		}
	}
?>
	<div class="container">
		<h1 class="d-block w-100">Buscar</h1>
		<div class="line d-block"></div>
		<form id="frm-buscar" method="post" action="" class="align-center mt-4 mb-4">
			<div class="form-group">
				<input type="text" class="form-control w250 inline-block v-top" name="termino" id="termino" value="<?php echo $termino; ?>" placeholder="T&eacute;rmino..." required="required" />
				<button name="buscar" type="submit" class="btn inline-block v-top">BUSCAR</button>
			</div>
		</form>
		<?php
			if(!isset($_POST['buscar'])){
				echo "<label class=''>Lo invitamos a buscar una pel&iacute;cula, puede buscar tanto por el nombre de la misma como por alg&uacute;n t&eacute;rmino de la sinopsis.</label>";
			}
			if(isset($mensajeBusqueda)){
				echo "<label class='error'>" . $mensajeBusqueda . "</label>";
			}
			if($isValid == true){
				echo "<label class='success'>Se han encontrado las siguientes pel&iacute;culas</label>";
		?>
			<div class="row listado-peliculas">
				<?php
					while($pelicula = mysqli_fetch_assoc($peliculas_query)) {
				?>
					<div class="col-xl-2 col-lg-4">
						<a href="pelicula.php?id=<?php echo $pelicula['id']; ?>"><img src="<?php echo $pelicula['portada']; ?>" alt="<?php echo $pelicula['nombre']; ?>" /></a><br />
						<a href="pelicula.php?id=<?php echo $pelicula['id']; ?>"><?php echo $pelicula['nombre']; ?></a>
					</div>
				<?php
					}
				?>
			</div>
		<?php
			}
		?>
	</div>
<?php
	require "footer.php";
?>