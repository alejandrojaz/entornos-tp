$(document).ready(function(){
	if($('.carousel').length > 0){
		$('.carousel').carousel();
	}
});

$(document).on('click', '.btn-comprar', function(){
	var id_pelicula = $(this).attr('data-id');
	$.ajax({
		type: "POST",
		url: 'agregar-carrito.php',
		data: {id_pelicula: id_pelicula},
		success: function(result){
			if(result == "success"){
				alert("Producto agregado al carrito correctamente");
			}
			else{
				alert("Se produjo un error, intente en unos instantes");
			}
		}
	});
});