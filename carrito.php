<?php
	require "header.php";
	$isValid = false;
	// Si está seteado están sacando película del carrito de compras
	if(isset($_GET['id_pelicula'])){
		$id_pelicula = $_GET['id_pelicula'];
		if(isset($_SESSION['carrito'][$id_pelicula])){
			unset($_SESSION['carrito'][$id_pelicula]);
		}
	}
	$ids_peliculas = "";
	foreach($_SESSION['carrito'] as $k => $v){
		$ids_peliculas .= ($ids_peliculas == "") ? $k : "," . $k;
	}
	if($ids_peliculas != ""){
		$peliculas_query = mysqli_query($con, "select * from pelicula where id in($ids_peliculas)");
		if(mysqli_num_rows($peliculas_query) > 0){
			$isValid = true;
		}
	}
	$total = 0;
?>
	<div class="container">
		<h1 class="d-block w-100">Mi carrito</h1>
		<div class="line d-block"></div>
		<table class="table mt-4" border="0">
			<thead class="thead-light">
				<tr>
					<th scope="col" class="w100">Cantidad</th>
					<th scope="col" class="w150">Precio individual</th>
					<th scope="col" class="w100">Precio</th>
					<th scope="col">Nombre</th>
					<th scope="col" class="w100">Acciones</th>
				</tr>
			</thead>
			<tbody>
				<?php
					if($isValid == true){
						while($pelicula = mysqli_fetch_assoc($peliculas_query)){
							$cantidad = $_SESSION['carrito'][$pelicula['id']]['cantidad'];
							$precio_individual = $_SESSION['carrito'][$pelicula['id']]['precio'];
							$precio = round($precio_individual * $cantidad, 2);
							$total += $precio;
							echo "<tr><td>" . $cantidad . "</td><td>$ " . $precio_individual . "</td><td>$ " . $precio . "</td><td>" . $pelicula['nombre'] . "</td><td class='align-right'><a href='carrito.php?id_pelicula=" . $pelicula['id'] . "' title='Eliminar' onClick='return confirm(\"Seguro desea quitar del carrito la pel&iacute;cula?\");'><i class='fa fa-trash-alt'></i></a></td></tr>";
						}
					}
				?>
			</tbody>
		</table>
		<?php
			if($total != 0){
				echo "<label>El total es: $ " . $total . "</label><br />";
				if($_SESSION['tipo_usuario'] == "cliente"){
					echo '<a href="finalizar-compra.php" class="btn">FINALIZAR COMPRA</a>';
				}
				else{
					echo "<label><strong>Tienes que estar logueado para poder finalizar la compra</strong></label><br />";
				}
			}
		?>
		<br />
	</div>
<?php
	require "footer.php";
?>